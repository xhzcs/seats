// pages/zwyd/zwyd.js
const db=wx.cloud.database()
var times=require('../../utils/times.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list_id:'',
    rmbs:'',
    userinfo:'',
    openid:'',
    list:'',
    sjxx:''
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userinfo = wx.getStorageSync('userinfo')
    this.setData({
      list_id:options.list_id, 
      userinfo:userinfo,
      openid:userinfo[0].openid
    })
    wx.cloud.callFunction({
      name:'getzwlist',
      data:{
        listid:options.list_id
      },
      complete:res=>{
        console.log(res.result.list)
        this.setData({
          rmbs:res.result.list
        })
      }
    })
  },
  qrxz() {
      db.collection("xzxx").add({
        data:{
          openid:this.data.openid,
          zxzw:this.data.list_id,
          qdzt:0,
          qt:0,
          _createTime:Date.parse(new Date())
        }
      }).then(res=>{
        wx.showToast({
          title: '选座成功',
          icon:'success',
          duration:2000,
          success:()=>{
            db.collection("tsgzw").doc(this.data.list_id).update({
              data:{
                zwzt:2
              },
            success:res=>{
              setTimeout(function(){
                wx.navigateTo({
                  url: '../dkqd/dkqd',
                })
              },2000)
            }
            })
          }
        })
  })  
  },
  qrxz1(){
    const app=getApp() 
    var id=app.globalData.openid
    wx.cloud.callFunction({
      name:'getxz',
      data:{
        openid:id
      },
     complete:res=>{
       console.log(res.result.list)
       if(res.result.list[0].qt==1){
          // console.log(res.result.list[1].qt)
          // console.log("true")
          db.collection("xzxx").add({
            data:{
              openid:this.data.openid,
              zxzw:this.data.list_id,
              qdzt:0,
              qt:0,
              _createTime:Date.parse(new Date())
            }
          }).then(res=>{
            wx.showToast({
              title: '选座成功',
              icon:'success',
              duration:2000,
              success:()=>{
                db.collection("tsgzw").doc(this.data.list_id).update({
                  data:{
                    zwzt:2
                  },
                success:res=>{
                  setTimeout(function(){
                    wx.navigateTo({
                      url: '../dkqd/dkqd',
                    })
                  },2000)
                }
                })
              }
            })
      })  
       }else if(res.result.list[0].qt==0){
          wx.showToast({
            title: '请勿多选',
            icon:  'error'
          })
        }
     }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})