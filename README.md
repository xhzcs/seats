
#### 介绍
图书馆座位预约小程序，采用腾讯云开发，功能有座位预约，签到打卡，扫码签到，监督举报，失物招领，图书检索，新闻公告等功能
#### 请留下你的Star
#### 演示视频地址
 https://www.bilibili.com/video/BV1cMUkYEEwF/?spm_id_from=333.1387.homepage.video_card.click&vd_source=a3812e87c9528a5a17930a22537e9bec
#### 运行效果图

![输入图片说明](xgt/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20250201155413.jpg)
####部署说明
本项目只是小案例，完整版代码联系up主获取

##### 联系作者
微信：15121959400
欢迎各位开发讨论交流
